function [x,xL,xx,xxL,xxLL,Lfree] = EGFR_Species_TotalLigand(kx,L1,L2,L3,Ltotalconc,volume,Ravg,Rtotalconc,conc)

Ltotal = Ltotalconc.*volume;
Rtotal = Rtotalconc.*volume;

if Ltotalconc == 0
    Lfree = 0;
else
%First equation (Ltotal to Lfree), solve for xavg in terms of Lfree
syms xavg_sym Lfree_sym
eqn = (4*Rtotal*L3*L2*kx.*xavg_sym.^2.*Lfree_sym.^2)./Ravg + ...
     ((2*L2*kx.*xavg_sym.^2+L1.*xavg_sym)*Rtotal./Ravg+volume).*Lfree_sym - Ltotal == 0;
S = solve(eqn, xavg_sym);
xavg = S(2,1); 

%Substitute xavg into the second equation (total receptors) for the average cell, and solve for Lfree
syms Lfree_sym;
eqn = (2*kx+2*L2*kx*Lfree_sym+2*L3*L2*kx.*Lfree_sym.^2).*xavg.^2+(1+L1.*Lfree_sym).*xavg-Ravg==0;
Lfree_all = solve(eqn, Lfree_sym);
Lfree = max(real(double(Lfree_all)));
end

%Solve for x for the individual cell
x_roots = roots([2*kx+2*L2*kx*Lfree+2*L3*L2*kx.*Lfree.^2 1+L1.*Lfree -conc]);

%Determine the distribution of species
x = max(real(x_roots));
xL = L1.*Lfree.*x;
xx = 2*kx.*x.^2;
xxL = 2*L2*kx.*Lfree.*x.^2;
xxLL = 2*L3*L2*kx.*Lfree.^2.*x.^2;

end