# unliganded dimer correction

This repository contains the Matlab code used for the correction of dose response curves for basal EGFR phosphorylation as described in the paper "Quantification of ligand and mutation-induced bias in EGFR phosphorylation in direct response to ligand binding" by Daniel Wirth, Ece Özdemir and Kalina Hristova.

## Description 

Despite open questions about the association state of EGFR, it has been shown that its function can be described in quantitative terms by a thermodynamic cycle where EGFR can exist in its unliganded monomeric form x, unliganded dimeric form xx, single-liganded monomeric form xL, single-liganded dimeric form xxL, and double-liganded dimeric form xxLL. 

In our experiments, we measure the EGFR concentration in the membrane of each vesicle, _conc_. We also know the total EGFR concentration in the imaging dish, _Rtotalconc_, and the total ligand concentration in the imaging dish _Ltotalconc_. With these measurements and the known constants kx, L1, L2, L3 and the volume in the dish the code calculates the EGFR species distribution. 

For more details please see: "Quantification of ligand and mutation-induced bias in EGFR phosphorylation in direct response to ligand binding" by Daniel Wirth, Ece Özdemir and Kalina Hristova.

## License

The software is open source under the Apache-2.0 License. Feel free to use the software, but please cite its authors.
doi: https://doi.org/10.1101/2023.02.13.528340
